﻿using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Nuapay.Sdk.Interfaces
{
    public interface INuapayPaymentsApi
    {
        Task<CreatePaymentResult> CreatePayment(Payment payment, CancellationToken cancellationToken);
    }

    public class Payment
    {
        public decimal Amount { get; set; }
    }

    public class CreatePaymentResult
    {
        public string PaymentId { get; set; }
        public string Error { get; set; }
    }
}
