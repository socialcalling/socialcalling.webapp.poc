﻿namespace Nuapay.Sdk.Models.Payments
{
    public class CreatePaymentRequest
    {
        public string Amount { get; set; }

        public string Currency { get; set; } = "GBP";

        public string CountryCode { get; set; } = "GB";
    }
}
