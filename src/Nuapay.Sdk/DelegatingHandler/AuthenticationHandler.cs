﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Nuapay.Sdk.DelegatingHandler
{
    public class AuthenticationHandler : System.Net.Http.DelegatingHandler  
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var key = "e4ef45d965108d1f95882d302773b8a7157ab241e469042452b422fd77b0b9a2"; //Get from config

            request.Headers.Authorization =
                new AuthenticationHeaderValue(
                    "Basic", Convert.ToBase64String(
                        System.Text.Encoding.ASCII.GetBytes(key)));

            return await base.SendAsync(request, cancellationToken);
        }
    }
}
