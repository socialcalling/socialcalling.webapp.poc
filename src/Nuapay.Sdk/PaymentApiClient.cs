﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Nuapay.Sdk.Interfaces;
using Nuapay.Sdk.Models.Payments;

namespace Nuapay.Sdk
{
    public class PaymentApiClient : INuapayPaymentsApi
    {
        private readonly HttpClient _httpClient;

        public PaymentApiClient(HttpClient httpClient)
        {
            //TODO: configure
            httpClient.BaseAddress = new Uri("https://sandbox.nuapay.com/tpp/");

            _httpClient = httpClient;
        }


        public async Task<CreatePaymentResult> CreatePayment(
            Payment payment, 
            CancellationToken cancellationToken)
        {
            var request = new CreatePaymentRequest
            {
                Amount = payment.Amount.ToString("F")
            };

            var serializeObject = JsonConvert.SerializeObject(request, ConfigureJsonSerializerSettings());

            var content = new StringContent(
                serializeObject, 
                Encoding.UTF8, 
                "application/json");

            var responseMessage = await _httpClient.PostAsync(
                "payments", 
                content, 
                cancellationToken);

            if (responseMessage.IsSuccessStatusCode)
            {
                var body = JsonConvert.DeserializeObject<CreatePaymentResponse>(await responseMessage.Content.ReadAsStringAsync());

                return new CreatePaymentResult
                {
                    PaymentId = body.Data.Id
                };
            }

            return new CreatePaymentResult
            {
                Error = await responseMessage.Content.ReadAsStringAsync()
            };
        }

        public static JsonSerializerSettings ConfigureJsonSerializerSettings()
        {
            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settings.NullValueHandling = NullValueHandling.Ignore;
            return settings;
        }
    }

    public class CreatePaymentResponse
    {
        public string Uri { get; set; }

        public CreatePaymentData Data { get; set; }
    }

    public class CreatePaymentData
    {
        public string Id { get; set; }
    }
}