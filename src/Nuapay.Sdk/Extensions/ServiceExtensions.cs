﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nuapay.Sdk.DelegatingHandler;
using Nuapay.Sdk.Interfaces;

namespace Nuapay.Sdk.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddNuapayApi(
            this IServiceCollection services, 
            IConfigurationSection configuration)
        {
            services.AddHttpClient<INuapayPaymentsApi, PaymentApiClient>()
                .AddHttpMessageHandler<AuthenticationHandler>();

            services.AddTransient<AuthenticationHandler>();
        }
    }
}
