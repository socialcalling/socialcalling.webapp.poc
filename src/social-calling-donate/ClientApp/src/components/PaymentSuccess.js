import React, { Component } from "react";
import { Grid, Icon } from "semantic-ui-react";

export class PaymentSuccess extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div>
        <Grid centered columns="1" padded="vertically">
          <Grid.Row>
            <Grid.Column>
              <h1>Thank you</h1>
              <h2>Your donation has been sent directly to the charity!</h2>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column textAlign="right">
              <Icon fitted name="lock" /> secure bank transfer with
              <a href="https://the-social-pay-test.webflow.io/">
                the social pay
              </a>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}
