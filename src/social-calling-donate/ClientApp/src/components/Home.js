import React, { Component } from "react";
import { Button, Input, Grid, Icon } from "semantic-ui-react";
import "./Home.css";

export class Home extends Component {
  static displayName = Home.name;
  static nuapayEndpoint = "https://sandbox.nuapay.com/tpp-ui/";

  constructor(props) {
    super(props);
    this.state = {
      amount: 20,
      loading: false,
      otherAmount: false,
      hasErrors: false,
    };
    this.donateClick = this.donateClick.bind(this);
    this.quickDonateClick = this.quickDonateClick.bind(this);
    this.otherAmountOnChange = this.otherAmountOnChange.bind(this);
    this.nuapayEvent = this.nuapayEvent.bind(this);
    window.addEventListener("message", this.nuapayEvent, false);
  }

  componentWillUnmount(){
    window.NuapayOpenBanking.closeUI();
    window.removeEventListener("message", this.nuapayEvent, false);
  }

  componentDidCatch(error, info) {
    console.error(error);
  }

  render() {
    return (
      <div>
        <Grid centered columns="1" padded="vertically">
          <Grid.Row>
            <Grid.Column>
              <Button.Group basic fluid>
                <Button
                  className={
                    this.state.amount === 20 && !this.state.otherAmount
                      ? "active"
                      : ""
                  }
                  onClick={this.quickDonateClick}
                >
                  £20
                </Button>
                <Button
                  className={
                    this.state.amount === 50 && !this.state.otherAmount
                      ? "active"
                      : ""
                  }
                  onClick={this.quickDonateClick}
                >
                  £50
                </Button>
                <Button
                  className={
                    this.state.amount === 100 && !this.state.otherAmount
                      ? "active"
                      : ""
                  }
                  onClick={this.quickDonateClick}
                >
                  £100
                </Button>
                <Button
                  className={this.state.otherAmount ? "active" : ""}
                  onClick={this.quickDonateClick}
                >
                  Other
                </Button>
              </Button.Group>
            </Grid.Column>
          </Grid.Row>

          {this.state.otherAmount && (
            <Grid.Row>
              <Grid.Column>
                <Input
                  fluid
                  type="number"
                  placeholder="£ - other amount"
                  className="center"
                  onChange={this.otherAmountOnChange}
                  focus={this.state.otherAmount}
                />
              </Grid.Column>
            </Grid.Row>
          )}

          <Grid.Row>
            <Grid.Column>
              <Button
                primary
                fluid
                onClick={this.donateClick}
                loading={this.state.loading}
                disabled={this.state.loading && this.isValid()}
              >
                Donate £{this.state.amount}
              </Button>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column textAlign="right">
              <Icon fitted name="lock" /> secure bank transfer with
              <a href="https://the-social-pay-test.webflow.io/">
                the social pay
              </a>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }

  isValid() {
    return !isNaN(this.state.amount) && this.state.amount > 0;
  }

  otherAmountOnChange(event) {
    let amount = event.target.value;

    this.setState({
      amount: parseInt(amount || 20),
    });
  }

  nuapayEvent(event) {
    window.NuapayOpenBanking.closeUI();

    console.log(`Nuapay status ${event.data.status}`);

    switch (event.data.status) {
      case "CLOSED":
        this.setState({
          loading: false,
        });
        break;
      case "COMPLETE":
        this.props.history.push("/success");
        break;
      default:
        this.setState({
          loading: false,
        });
    }
  }

  async donateClick(event, data) {
    this.setState({
      loading: true,
    });

    await this.makeDonation();

    window.NuapayOpenBanking.showUI(this.state.paymentId, this.nuapayEndpoint);
  }

  async quickDonateClick(event, data) {
    if (data.children === "Other") {
      this.setState({
        otherAmount: true,
      });
      return;
    } else {
      this.setState({
        otherAmount: false,
      });
    }

    let amount = data.children.replace(/\D/g, "");

    this.setState({
      amount: parseInt(amount),
    });
  }

  async makeDonation() {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ amount: this.state.amount }),
    };
    fetch("api/payments", requestOptions)
      .then((response) => response.json())
      .then((data) => {
        window.NuapayOpenBanking.showUI(
          data.paymentId,
          "https://sandbox.nuapay.com/tpp-ui/"
        );
      })
      .catch(() => this.setState({ hasErrors: true }));
  }
}
