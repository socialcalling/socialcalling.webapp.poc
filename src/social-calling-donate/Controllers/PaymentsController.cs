﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Nuapay.Sdk.Interfaces;

namespace social_calling_donate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly INuapayPaymentsApi _nuapay;

        public PaymentsController(
            INuapayPaymentsApi nuapay
            )
        {
            _nuapay = nuapay;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public string Get()
        {
            return "bob";
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CreatePaymentsResponse>> Create(
            CreatePaymentsRequest request,
            CancellationToken cancellationToken)
        {
            var result = await _nuapay.CreatePayment(new Payment
            {
                Amount = request.Amount
            }, cancellationToken);

            return Created(result.PaymentId ?? "yeah", new CreatePaymentsResponse
            {
                PaymentId = result.PaymentId,
                Error = result.Error
            });
        }
    }

    public class CreatePaymentsRequest
    {
        public decimal Amount { get; set; }
    }

    public class CreatePaymentsResponse
    {
        public string PaymentId { get; set; }
        public string Error { get; set; }
    }
}